FROM alpine:3.14

RUN apk --no-cache add openjdk8
RUN apk --no-cache add nodejs yarn
RUN apk --no-cache add python3 py3-pip
RUN java -version && node --version && yarn --version

CMD ["/bin/sh"]
